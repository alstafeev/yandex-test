package com.astafeev;

import com.astafeev.config.Config;
import com.astafeev.objects.enums.LoadTypeEnum;
import com.astafeev.utils.WaitingUtils;
import java.util.Scanner;
import java.util.regex.Pattern;
import lombok.extern.log4j.Log4j;

/**
 * Class for demo run
 */
@Log4j
public class DeliveryPriceDemo {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Pattern loadPattern = Pattern.compile("(default)|(veryhigh)|(high)|(increased)",
        Pattern.CASE_INSENSITIVE);

    log.info("Enter the distance to your destination");
    WaitingUtils.waitUntil(Config.getInstance().getWaitTimeout(), Config.getInstance().getPollTimeout(),
        sc::hasNextDouble);
    double distance = sc.nextDouble();

    log.info("Is oversize cargo?");
    WaitingUtils.waitUntil(Config.getInstance().getWaitTimeout(), Config.getInstance().getPollTimeout(),
        sc::hasNextBoolean);
    boolean isOversize = sc.nextBoolean();

    log.info("Is cargo fragile?");
    WaitingUtils.waitUntil(Config.getInstance().getWaitTimeout(), Config.getInstance().getPollTimeout(),
        sc::hasNextBoolean);
    boolean isFragile = sc.nextBoolean();

    log.info("Is shipping is loaded? (enter one of: 'default', 'veryhigh', 'high', 'increased')");
    WaitingUtils.waitUntil(Config.getInstance().getWaitTimeout(), Config.getInstance().getPollTimeout(),
        () -> sc.hasNext(loadPattern));
    LoadTypeEnum loadType = LoadTypeEnum.getByName(sc.next(loadPattern).toLowerCase());

    DeliveryPriceCalculator calculator = new DeliveryPriceCalculator(distance, isOversize, isFragile, loadType);
    log.info(String.format("Delivery price: %d", calculator.calculate()));
  }
}

package com.astafeev.objects;

import lombok.Data;

/**
 * Class for load coefficient
 */
@Data
public class LoadCoefficient {

  private double defaultCoefficient;
  private double veryHighCoefficient;
  private double highCoefficient;
  private double increasedCoefficient;
}

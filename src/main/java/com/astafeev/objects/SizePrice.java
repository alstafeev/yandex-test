package com.astafeev.objects;

import lombok.Data;

/**
 * Class for size price
 */
@Data
public class SizePrice {

  private int big;
  private int small;
}

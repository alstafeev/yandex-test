package com.astafeev.objects;

import lombok.Data;

/**
 * Class for distance price
 */
@Data
public class DistancePrice {

  private int more30km;
  private int less30km;
  private int less10km;
  private int less2km;
}

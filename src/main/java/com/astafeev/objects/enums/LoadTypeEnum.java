package com.astafeev.objects.enums;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Load types
 */
@AllArgsConstructor
@Getter
public enum LoadTypeEnum {

  DEFAULT("default"),
  VERYHIGH("veryhigh"),
  HIGH("high"),
  INCREASED("increased");

  private static final Map<String, LoadTypeEnum> types = new HashMap<>();

  static {
    for (LoadTypeEnum value : values()) {
      types.put(value.value, value);
    }
  }

  private final String value;

  public static LoadTypeEnum getByName(String name) {
    return types.get(name);
  }
}

package com.astafeev.utils;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j;
import org.awaitility.Awaitility;

/**
 * Class for waiting some actions
 */
@Log4j
@UtilityClass
public class WaitingUtils {

  public static void waitUntil(long maxTimeout, long pollInterval, Callable<Boolean> action) {
    Awaitility
        .await()
        .atMost(maxTimeout, TimeUnit.SECONDS)
        .ignoreExceptions()
        .pollInSameThread()
        .pollInterval(pollInterval, TimeUnit.SECONDS)
        .until(action);
  }
}

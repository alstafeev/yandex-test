package com.astafeev;

import com.astafeev.config.Config;
import com.astafeev.exceptions.DistanceTooLongException;
import com.astafeev.objects.enums.LoadTypeEnum;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * Class for delivery price calculation
 */
@Log4j
@AllArgsConstructor
public final class DeliveryPriceCalculator {

  private final double distance;
  private final boolean isOversize;
  private final boolean isFragile;
  private final LoadTypeEnum loadType;

  public int calculate() {
    int finalPrice = 0;
    finalPrice = finalPrice + calculateDestinationPrice();
    finalPrice += calculateSizePrice();
    finalPrice += calculateFragilePrice();
    finalPrice *= calculateLoadPrice();
    return Math.max(finalPrice, Config.getInstance().getDefaultPrice());
  }

  private double calculateLoadPrice() {
    switch (loadType) {
      case INCREASED:
        return Config.getInstance().getLoadCoefficient().getIncreasedCoefficient();
      case HIGH:
        return Config.getInstance().getLoadCoefficient().getHighCoefficient();
      case VERYHIGH:
        return Config.getInstance().getLoadCoefficient().getVeryHighCoefficient();
      case DEFAULT:
      default:
        return Config.getInstance().getLoadCoefficient().getDefaultCoefficient();
    }
  }

  private int calculateFragilePrice() {
    if (distance > 30 && isFragile) {
      throw new DistanceTooLongException("Fragile cargo cannot be shipped over long distances (over 30 km)");
    }
    return isFragile ? Config.getInstance().getFragilePrice() : 0;
  }

  private int calculateSizePrice() {
    return isOversize
        ? Config.getInstance().getSizePrice().getBig()
        : Config.getInstance().getSizePrice().getSmall();
  }

  private int calculateDestinationPrice() {
    if (distance > 0 && distance <= 2) {
      return Config.getInstance().getDistancePrice().getLess2km();
    }

    if (distance > 2 && distance <= 10) {
      return Config.getInstance().getDistancePrice().getLess10km();
    }

    if (distance > 10 && distance <= 30) {
      return Config.getInstance().getDistancePrice().getLess30km();
    }

    if (distance > 30) {
      return Config.getInstance().getDistancePrice().getMore30km();
    }
    throw new IllegalArgumentException("Distance cannot be negative");
  }
}

package com.astafeev.config;

import com.astafeev.objects.DistancePrice;
import com.astafeev.objects.LoadCoefficient;
import com.astafeev.objects.SizePrice;
import java.io.InputStream;
import java.util.Properties;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;

/**
 * Class to get the properties from com.astafeev.config file
 */
@Log4j
public class Config {

  private static Config config;
  private final Properties props;
  private final DistancePrice distancePrice = new DistancePrice();
  private final SizePrice sizePrice = new SizePrice();
  private final LoadCoefficient loadCoefficient = new LoadCoefficient();

  private Config() {
    props = new Properties();
    loadProperties();
    createPrices();
  }

  public static Config getInstance() {
    if (config == null) {
      config = new Config();
    }
    return config;
  }

  @SneakyThrows
  private void loadProperties() {
    try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties")) {
      props.load(is);
    }
  }

  private void createPrices() {
    distancePrice.setMore30km(Integer.parseInt(props.getProperty("distance.more30km")));
    distancePrice.setLess30km(Integer.parseInt(props.getProperty("distance.less30km")));
    distancePrice.setLess10km(Integer.parseInt(props.getProperty("distance.less10km")));
    distancePrice.setLess2km(Integer.parseInt(props.getProperty("distance.less2km")));

    sizePrice.setBig(Integer.parseInt(props.getProperty("size.big")));
    sizePrice.setSmall(Integer.parseInt(props.getProperty("size.small")));

    loadCoefficient.setHighCoefficient(Double.parseDouble(props.getProperty("load.high")));
    loadCoefficient.setVeryHighCoefficient(Double.parseDouble(props.getProperty("load.veryhigh")));
    loadCoefficient.setIncreasedCoefficient(Double.parseDouble(props.getProperty("load.increased")));
    loadCoefficient.setDefaultCoefficient(Double.parseDouble(props.getProperty("load.default")));
  }

  public DistancePrice getDistancePrice() {
    return distancePrice;
  }

  public SizePrice getSizePrice() {
    return sizePrice;
  }

  public int getDefaultPrice() {
    return Integer.parseInt(props.getProperty("price.default"));
  }

  public LoadCoefficient getLoadCoefficient() {
    return loadCoefficient;
  }

  public long getWaitTimeout() {
    return Long.parseLong(props.getProperty("timeout.wait"));
  }

  public long getPollTimeout() {
    return Long.parseLong(props.getProperty("timeout.poll"));
  }

  public int getFragilePrice() {
    return Integer.parseInt(props.getProperty("price.fragile"));
  }
}

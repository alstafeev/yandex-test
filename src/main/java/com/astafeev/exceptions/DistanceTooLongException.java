package com.astafeev.exceptions;

/**
 * Exception for fragile cargo when shipped over long distances
 */
public class DistanceTooLongException extends RuntimeException {

  public DistanceTooLongException(String message) {
    super(message);
  }
}

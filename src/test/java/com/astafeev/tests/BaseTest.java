package com.astafeev.tests;

import com.astafeev.exceptions.ReadDataProviderSourceException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.File;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

/**
 * Base class for tests
 */
@Log4j
public class BaseTest {

  protected static final Pattern CAPITAL_PATTERN = Pattern.compile("[A-Z]");

  @SneakyThrows
  @DataProvider(parallel = true)
  public static Object[][] factoryObjects(ITestContext context) {
    Class<?> clazz = context.getCurrentXmlTest().getClasses().get(0).getSupportClass();
    String path = CAPITAL_PATTERN.matcher("src/test/java/" + clazz.getName()
            .replace(".", "/")
            .replace("Test", "ObjectsF.json"))
        .replaceFirst(clazz.getSimpleName().substring(0, 1).toLowerCase(Locale.getDefault()));
    Class<?>[] params = Arrays.stream(clazz.getDeclaredConstructors())
        .filter(c -> "factoryObjects".equals(c.getAnnotation(Factory.class).dataProvider()))
        .findFirst()
        .orElseThrow(IllegalArgumentException::new)
        .getParameterTypes();
    return readObjectsFromFile(path, params);
  }

  @SneakyThrows
  protected static Object[][] readObjectsFromFile(String path, Class<?>[] params) {
    try {
      List<Class<?>> obj = Arrays.asList(params);

      Configuration cfg = new Configuration(Configuration.VERSION_2_3_30);
      cfg.setDirectoryForTemplateLoading(new File(path.substring(0, path.lastIndexOf('/'))));
      cfg.setDefaultEncoding("UTF-8");
      cfg.setNumberFormat("#");
      cfg.setTimeZone(TimeZone.getTimeZone("GMT"));
      Template template = cfg.getTemplate(path.substring(path.lastIndexOf('/') + 1));
      Map<String, Object> templateData = new HashMap<>();

      String json;

      try (StringWriter writer = new StringWriter()) {
        template.process(templateData, writer);
        json = writer.toString();
      }

      ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
      Object[][] dataForTest = objectMapper.readValue(json, Object[][].class);

      return Arrays.stream(dataForTest).map(e ->
          IntStream.range(0, obj.size())
              .mapToObj(i -> objectMapper.convertValue(e[i], obj.get(i)))
              .toArray()
      ).toArray(Object[][]::new);
    } catch (Exception e) {
      throw new ReadDataProviderSourceException(path, e);
    }
  }
}

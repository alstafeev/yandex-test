package com.astafeev.tests;

import com.astafeev.DeliveryPriceCalculator;
import com.astafeev.objects.TestData;
import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.testng.Assert;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

/**
 * Tests for delivery price calculator
 */
@Log4j
public class DeliveryPriceTest extends BaseTest {

  private final JsonF jsonF;

  @Factory(dataProvider = "factoryObjects")
  public DeliveryPriceTest(JsonF jsonF) {
    this.jsonF = jsonF;
  }

  @Test(testName = "Calculate delivery price")
  public void calculateDeliveryPrice() {
    log.info(jsonF.testData);
    DeliveryPriceCalculator deliveryPriceCalculator = new DeliveryPriceCalculator(jsonF.getTestData().getDistance(),
        jsonF.getTestData().isOversize(), jsonF.getTestData().isFragile(), jsonF.getTestData().getLoadType());
    Assert.assertEquals(deliveryPriceCalculator.calculate(), jsonF.getExpectedPrice(),
        "Delivery price wrong");
  }

  @Data
  private static class JsonF {

    private TestData testData;
    private int expectedPrice;
  }
}

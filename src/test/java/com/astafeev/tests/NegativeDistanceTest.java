package com.astafeev.tests;

import com.astafeev.DeliveryPriceCalculator;
import com.astafeev.objects.TestData;
import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

/**
 * Tests for delivery to negative distance price calculator
 */
@Log4j
public class NegativeDistanceTest extends BaseTest {

  private final JsonF jsonF;

  @Factory(dataProvider = "factoryObjects")
  public NegativeDistanceTest(JsonF jsonF) {
    this.jsonF = jsonF;
  }

  @Test(testName = "Calculate delivery price",
      expectedExceptions = {IllegalArgumentException.class},
      expectedExceptionsMessageRegExp = "Distance cannot be negative")
  public void calculateDeliveryPrice() {
    log.info(jsonF.testData);
    DeliveryPriceCalculator deliveryPriceCalculator = new DeliveryPriceCalculator(jsonF.getTestData().getDistance(),
        jsonF.getTestData().isOversize(), jsonF.getTestData().isFragile(), jsonF.getTestData().getLoadType());
    deliveryPriceCalculator.calculate();
  }

  @Data
  private static class JsonF {

    private TestData testData;
  }
}

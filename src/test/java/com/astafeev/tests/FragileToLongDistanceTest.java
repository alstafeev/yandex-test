package com.astafeev.tests;

import com.astafeev.DeliveryPriceCalculator;
import com.astafeev.exceptions.DistanceTooLongException;
import com.astafeev.objects.TestData;
import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

/**
 * Tests for delivery fragile cargo to long distance price calculator
 */
@Log4j
public class FragileToLongDistanceTest extends BaseTest {

  private final JsonF jsonF;

  @Factory(dataProvider = "factoryObjects")
  public FragileToLongDistanceTest(JsonF jsonF) {
    this.jsonF = jsonF;
  }

  @Test(testName = "Calculate delivery price",
      expectedExceptions = {DistanceTooLongException.class},
      expectedExceptionsMessageRegExp = "Fragile cargo cannot be shipped over long distances \\(over 30 km\\)")
  public void calculateDeliveryPrice() {
    log.info(jsonF.testData);
    DeliveryPriceCalculator deliveryPriceCalculator = new DeliveryPriceCalculator(jsonF.getTestData().getDistance(),
        jsonF.getTestData().isOversize(), jsonF.getTestData().isFragile(), jsonF.getTestData().getLoadType());
    deliveryPriceCalculator.calculate();
  }

  @Data
  private static class JsonF {

    private TestData testData;
  }
}

package com.astafeev.objects;

import com.astafeev.objects.enums.LoadTypeEnum;
import lombok.Data;

/**
 * Initial test data for tests
 */
@Data
public class TestData {

  private double distance;
  private boolean isOversize;
  private boolean isFragile;
  private LoadTypeEnum loadType;

  @Override
  public String toString() {
    return String.format("Calculate delivery price for distance - %4.3f oversize - %b fragile - %b load - %s",
        distance, isOversize, isFragile, loadType.getValue());
  }
}

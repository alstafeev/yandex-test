package com.astafeev.exceptions;

/**
 * Class for exceptions while reading data provider json files
 */
public class ReadDataProviderSourceException extends RuntimeException {

  public ReadDataProviderSourceException(String path, Throwable cause) {
    super(String.format("Problem with file %s", path), cause);
  }
}
